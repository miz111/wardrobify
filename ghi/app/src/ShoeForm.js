import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            modelName: '',
            color: '',
            bin: '',
            bins: []
          };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }
    

    async componentDidMount() {
        const binUrl = 'http://localhost:8100/api/bins/';

        const response = await fetch(binUrl);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({bins: data.bins });
            console.log(data.bins);
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.model_name = data.modelName
        delete data.modelName;
        delete data.bins;
        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            const cleared = {
                manufacturer: '',
                modelName: '',
                color: '',
                bin: '',
              };
              this.setState(cleared);


        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Shoe</h1>
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value = {this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                                <label htmlFor="name">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value = {this.state.modelName} placeholder="Model Name" required type="text" name="modelName" id="model_name" className="form-control" />
                                <label htmlFor="room_count">Model Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value = {this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="room_count">Color</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value = {this.state.bin} required name="bin" id="bin" className="form-select">
                                    <option value="">Select a bin</option>
                                    {this.state.bins.map(bin => {
                                        return (
                                            <option key={bin.id} value={bin.id}>
                                                {bin.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}
export default ShoeForm;
