import React, { useState, useEffect } from 'react'

const ShoesList = () => {
    const [shoes, setShoes] = useState([])


    const getShoes = async () => {
        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const response = await fetch(shoesUrl);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setShoes(data);
        }
    }

    const deleteShoe = async(id) => {
        const url = (`http://localhost:8080/api/shoes/${id}/`)
        console.log(url)
        const fetchConfig = {
             method: "delete",  
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            getShoes();
            const data = await response.json()
            console.log(data)
        }
    }

    useEffect(() => {
        getShoes();
    }, [])

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">manufacturer</th>
                        <th scope="col">Model Name</th>
                        <th scope="col">Color</th>
                        <th scope="col">Bin</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.shoes?.map(shoe => {
                        return (
                            <tr key={shoe.id}>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.bin}</td>
                                <td><button type="button" className="btn btn-danger" onClick={() => deleteShoe(shoe.id)}>Delete</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}


export default ShoesList;