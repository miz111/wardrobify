# Wardrobify

Team:

* Murad Khudiev - Shoes
* Minting Zhang - Hats

## Design

Wardrobe application running on server 3000:3000 and includes two microservices, which are called hats running on port 8090 and shoes running on port 8080. The backend of the application was built using wardrobe API and included creating models, views and urls. Application frontend was built through React library.

## Shoes microservice

- running on port 8080, the url path for which is being pulled and fetched from the server using the react functionality to return the actual output for the client on the frontend. The microservice had two models, BinVO and Shoe, Shoe has a ForeignKey relationship with BinVO. Views were created using the models and allow the host to list, show, create, delete and update a shoe while using the encoders for allowing properties of all types to be passed in as well as the Bin object to which the shoe is attached.
- frontend running on server 3000 as a react component. Created a list form as well as create form for the shoe microservice which allow the user to create, list and delete shoes. The forms are routed through the App.js page which includes all the components of the projects and displaying the output on the web page



## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Instructions to Load Program:
- (You may create a separate directory on your device first to store the project with the following commands:
    - mkdir <<Name of Directory>>
    - cd <<Name of Directory>>)
- Navigate to GitLab at https://gitlab.com/mkhudiev99/microservice-two-shot-2
- Clone the project using the HTTPS link.
    - In terminal, run: git clone <<paste HTTP link here>>
    - CD into project directory:
        - To verify the name of the directory, run "ls" in terminal
        - Then, run in terminal: cd microservices-two-shot-2
- To set up Docker, run the following commands (make sure you are still in your project directory):
    - docker volume create pgdata
    - docker-compose build (or docker compose build)
    - docker-compose up (or docker compose up)
        - There should be 7 containers running. Verify that they are running before proceeding.
    - Go to Wardrobe container and create a superuser.
        - Run: python manage.py createsuperuser
    - Log onto http://localhost:8100/admin/ and create at least 1 bin and 1 location before proceeding (for testing purposes).



Diagram of Project:

Please look in: ghi/app/public/PracticeProjectDiagram.png

URL's and Ports:
- Hats can be accessed on port 8090.
- Shoes can be accessed on port 8080.
- Wardrobe can be access on port 8100.
- React is hosted on 3000.
To load web page on browser:
Home page can be accessed via: http://localhost: 3000
List of hats can be accessed via: http://localhost:3000/hats
Form to add a hat can be accessed via: http://localhost:3000/hats/new


CRUD Route Documentation:
These can be run in Insomnia:
To create a new location:
- Send a POST request to: http://localhost:8100/api/locations/
    Example JSON body for creating a location:
    {
        "closet_name": "Closet C",
        "section_number": "1",
        "shelf_number": "7"
    }
    The response should mirror the input JSON body.

To get a list of locations:
- Send a GET request to: http://localhost:8100/api/locations/
    Example return:
    {
	"locations": [
		{
			"href": "/api/locations/2/",
			"id": 2,
			"closet_name": "Closet A",
			"section_number": 2,
			"shelf_number": 6
		}
	]
    }


To create a new hat:
- Send a POST request to: http://localhost:8090/api/hats/
    Example JSON body for creating a hat (For location, input a location id. You can obtain this from viewing the list of locations from doing the previous request):
    {
        "fabric": "straw",
        "style": "hawaiian",
        "color": "pink",
        "location": 1
    }
    The response should mirror the input JSON body.


To get a list of hats:
- Send a GET request to: http://localhost:8090/api/hats/
    Example return:
    {
        "hats": [
            {
                "fabric": "Cotton",
                "style": "Baseball",
                "color": "Red",
                "id": 47,
                "location": "Closet A"
            },
            {
                "fabric": "rainbow",
                "style": "Party",
                "color": "yellow",
                "id": 45,
                "location": "Closet A"
            }
        ]
    }


To get details on one hat:
- Send a GET request to: http://localhost:8090/api/hats/id/, replacing "id" with the actual id of a hat. You can view the id's of all hats when you send a request to get the list of hats.
    Example return:
    {
        "fabric": "Cotton",
        "style": "Baseball",
        "color": "Red",
        "picture_url": "https://cdn-ssl.s7.disneystore.com/is/image/DisneyShopping/2016056685364?fmt=jpeg&qlt=90&wid=608&hei=608",
        "id": 47,
        "location": {
            "closet_name": "Closet A",
            "import_href": "/api/locations/2/"
        }
    }

To update a hat:
- Send a PUT request to: http://localhost:8090/api/hats/id/, replacing "id" with the actual id of a hat. You can view the id's of all hats when you send a request to get the list of hats.
    Example inital Details on a hat:
    {
        "fabric": "paper",
        "style": "beanie",
        "color": "yellow",
        "picture_url": "https://m.media-amazon.com/images/I/91hwXzNkOpL._AC_SR175,263_QL70_.jpg",
        "id": 53,
        "location": {
            "closet_name": "Closet A",
            "import_href": "/api/locations/2/"
        }
    }

    Example JSON body for updating a hat:
    {
	"fabric" : "cotton"
    }

    Example output following above change:
    {
            {
        "fabric": "cotton",
        "style": "beanie",
        "color": "yellow",
        "picture_url": "https://m.media-amazon.com/images/I/91hwXzNkOpL._AC_SR175,263_QL70_.jpg",
        "id": 53,
        "location": {
            "closet_name": "Closet A",
            "import_href": "/api/locations/2/"
        }
    }
    }
    ** Notice how the value of fabric has been changed from paper to cotton. **

To delete a hat:
- Send a DELETE request to： http://localhost:8090/api/hats/id/, replacing "id" with the actual id of a hat. You can view the id's of all hats when you send a request to get the list of hats.
    Upon successful deletion, output should be:
    {
	    "deleted": true
    }
    If deletion was not successful, output should be:
    {
        "deleted": false
    }

Models
- For hats, there are two models being used, one for hats and one for location.
    - The LocationVO model has the following fields: import_href (provides a unique identifier), closet_name, section_number, and shelf_number.
    - The Hat model has the following fields: fabric, style, color, picture_url.
    The fields are important because they are used to either identify or describe an object. For example, the hat model specifies that a picture URL requires an input of a URL. If a user was to use anything other than a working URL, it will flag the input. Location is a value object.

Identification of Value Objects:

Location is a value object in this project.

As each hat is unique and has a life cycle (can be deleted), each hat is an entity. It is needed because without a location, you can't have a hat. Each hat needs to be stored in a location. Because they are needed, I have set the model to have hats within a location be deleted if that location is deleted. This is reflected by line 30 in hats/api/hats_rest/models (on_delete=models.CASCADE,).
